﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace FindTheOddInt.Tests
{
    [TestClass()]
    public class KataTests
    {
        [TestMethod()]
        public void FindItTest()
        {
            Assert.AreEqual(5, Kata.FindIt(new[] { 20, 1, -1, 2, -2, 3, 3, 5, 5, 1, 2, 4, 20, 4, -1, -2, 5 }));
        }

        [TestMethod]
        public void FindItTest_NoResult()
        {
            Assert.AreEqual(-1, Kata.FindIt(new[] { 1, 1, 2, 2 }));
        }

        [TestMethod]
        public void FindItTest_null()
        {
            Assert.AreEqual(-1, Kata.FindIt(null));
        }
    }
}