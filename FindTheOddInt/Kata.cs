﻿using System.Linq;

namespace FindTheOddInt
{
    public class Kata
    {
        public static int FindIt(int[] seq)
        {
            return seq?.GroupBy(x => x).SingleOrDefault(x => (x.Count() % 2).Equals(1))?.Key ?? -1;
        }
    }
}
